import argparse
import errno
import json
import logging
import os
import shutil
import sys
from os import path


def parse_arguments():
    parser = argparse.ArgumentParser(description='Standardize data types in pubmed json schema')
    parser.add_argument("source_path", help="Please specify path to pubmed input files to process", type=str)
    parser.add_argument("output_path", help="Please specify output path to write standardized pubmed files", type=str)
    return parser.parse_args()


def get_logger(filename, log_file=None):
    logger = logging.getLogger(filename)
    logger.setLevel(logging.INFO)

    formatter = logging.Formatter('%(asctime)s - [%(name)s:%(lineno)d] - %(levelname)s - %(message)s')

    channel = logging.FileHandler(log_file) if log_file else \
        logging.StreamHandler(sys.stdout)
    channel.setLevel(logging.INFO)
    channel.setFormatter(formatter)

    logger.addHandler(channel)

    return logger


logger = get_logger(__file__)
args = parse_arguments()
source_path = path.join(args.source_path, '')
output_path = path.join(args.output_path, '')


def write_to_file(content, target_dir, file_name):
    try:
        os.makedirs(target_dir)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    target_file = path.join(target_dir, file_name)
    with open(target_file, 'w') as outfile:
        json.dump(content, outfile)


shutil.rmtree(output_path, ignore_errors=True)

for file in os.listdir(source_path):
    source_file = source_path + file
    if os.path.isfile(source_file) and not file.startswith('.'):
        data = ''
        try:
            with open(source_file, 'r', encoding='utf8') as input_file:
                data = json.loads(input_file.read())
                article = data['PubmedArticle']['MedlineCitation']['Article']

                if 'Abstract' in article and 'AbstractText' in article['Abstract']:
                    abstractText = article['Abstract']['AbstractText']
                    if not isinstance(abstractText, list):
                        if isinstance(abstractText, str):
                            abstractTextList = {"Label": "UNLABELLED", "text": abstractText}
                        data['PubmedArticle']['MedlineCitation']['Article']['Abstract']['AbstractText'] = [
                            abstractTextList]

                if 'ChemicalList' in data['PubmedArticle']['MedlineCitation']:
                    chemicals = data['PubmedArticle']['MedlineCitation']['ChemicalList']['Chemical']
                    if not isinstance(chemicals, list):
                        data['PubmedArticle']['MedlineCitation']['ChemicalList']['Chemical'] = [chemicals]

                if 'CitationSubset' in data['PubmedArticle']['MedlineCitation']:
                    citationSubset = data['PubmedArticle']['MedlineCitation']['CitationSubset']
                    if not isinstance(citationSubset, list):
                        data['PubmedArticle']['MedlineCitation']['CitationSubset'] = [citationSubset]

                if 'AuthorList' in article:
                    authors = article['AuthorList']['Author']
                    if not isinstance(authors, list):
                        if 'AffiliationInfo' in authors:
                            affiliationInfo = authors['AffiliationInfo']
                            if not isinstance(affiliationInfo, list):
                                authors['AffiliationInfo'] = [affiliationInfo]
                        data['PubmedArticle']['MedlineCitation']['Article']['AuthorList']['Author'] = [authors]
                    else:
                        for i, f in enumerate(authors):
                            if 'AffiliationInfo' in f:
                                affiliationInfo = f['AffiliationInfo']
                                if not isinstance(affiliationInfo, list):
                                    authors[i]['AffiliationInfo'] = [affiliationInfo]
                        data['PubmedArticle']['MedlineCitation']['Article']['AuthorList']['Author'] = authors

                if 'PublicationTypeList' in article:
                    pubType = article['PublicationTypeList']['PublicationType']
                    if not isinstance(pubType, list):
                        data['PubmedArticle']['MedlineCitation']['Article']['PublicationTypeList'][
                            'PublicationType'] = [pubType]

                if 'GrantList' in article:
                    grant = article['GrantList']['Grant']
                    if not isinstance(grant, list):
                        data['PubmedArticle']['MedlineCitation']['Article']['GrantList']['Grant'] = [grant]

                if 'PubmedData' in data['PubmedArticle'] and 'ArticleIdList' in data['PubmedArticle']['PubmedData']:
                    articleIdList = data['PubmedArticle']['PubmedData']['ArticleIdList']
                    if 'ArticleId' in articleIdList:
                        articleId = articleIdList['ArticleId']
                        if not isinstance(articleId, list):
                            data['PubmedArticle']['PubmedData']['ArticleIdList']['ArticleId'] = [articleId]

                if 'CommentsCorrectionsList' in data['PubmedArticle']['MedlineCitation']:
                    commentsCorrectionList = data['PubmedArticle']['MedlineCitation']['CommentsCorrectionsList']
                    if 'CommentsCorrections' in commentsCorrectionList:
                        commentsCorrections = commentsCorrectionList['CommentsCorrections']
                        if not isinstance(commentsCorrections, list):
                            data['PubmedArticle']['MedlineCitation']['CommentsCorrectionsList'][
                                'CommentsCorrections'] = [commentsCorrections]

                if 'InvestigatorList' in data['PubmedArticle']['MedlineCitation']:
                    investigatorList = data['PubmedArticle']['MedlineCitation']['InvestigatorList']

                if 'MeshHeadingList' in data['PubmedArticle']['MedlineCitation']:

                    meshHeading = data['PubmedArticle']['MedlineCitation']['MeshHeadingList']['MeshHeading']

                    if not isinstance(meshHeading, list):
                        data['PubmedArticle']['MedlineCitation']['MeshHeadingList']['MeshHeading'] = [meshHeading]

                    for i, f in enumerate(data['PubmedArticle']['MedlineCitation']['MeshHeadingList']['MeshHeading']):

                        if 'QualifierName' in f:
                            if not isinstance(f["QualifierName"], list):
                                data['PubmedArticle']['MedlineCitation']['MeshHeadingList'][
                                    'MeshHeading'][i]["QualifierName"] = [f['QualifierName']]

                        if 'DescriptorName' in f:
                            if not isinstance(f['DescriptorName'], list):
                                data['PubmedArticle']['MedlineCitation']['MeshHeadingList']['MeshHeading'][i][
                                    "DescriptorName"] = [f['DescriptorName']]

                write_to_file(data, output_path, file)

        except Exception as e:
            logger.info("Unable to process file: {} : {}".format(file, e, e.with_traceback()))
