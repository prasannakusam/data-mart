use dfs.maprdb;

create table dfs.maprdb.struct_ind_conditions (drugbank_id, condition_meddra_id, cond_mod_of_meddra_id, cond_related_concepts, cond_assoc_with) as 

select 
ind.drugbank_id, 
a.struct_ind_flat.`condition`.meddra_id as condition_meddra_id,
a.struct_ind_flat.`condition`.modification_of.base.meddra_id as cond_mod_of_meddra_id,
a.struct_ind_flat.`condition`.related_concepts as cond_related_concepts,
a.struct_ind_flat.`condition_associated_with` as cond_assoc_with

from
dfs.maprdb.ind as ind
LEFT join (SELECT drugbank_id, flatten(ind.pharmacology.`structured_indications`) as struct_ind_flat 
from dfs.maprdb.ind ind) a 
on ind.drugbank_id=a.drugbank_id
where a.struct_ind_flat.`off_label`=false;



create or replace view dfs.maprdb.struct_cond_related_concepts as 

select a.drugbank_id, a.condition_meddra_id, a.cond_mod_of_meddra_id,
b.rel_con_flat.meddra_id as rel_cond_meddra_id, a.cond_assoc_with

from dfs.maprdb.struct_ind_conditions a

left join 
(select drugbank_id, flatten(cond_related_concepts) as rel_con_flat from dfs.maprdb.struct_ind_conditions) b
on a.drugbank_id=b.drugbank_id;



create or replace view dfs.maprdb.struct_cond_assoc_with as

select a.drugbank_id, a.condition_meddra_id, a.cond_mod_of_meddra_id, a.rel_cond_meddra_id, 
b.cond_assoc_with_flat.meddra_id as cond_assoc_with_meddra_id, 
b.cond_assoc_with_flat.modification_of.base.meddra_id as cond_assoc_with_mod_of_meddra_id, 
b.cond_assoc_with_flat.related_concepts as cond_assoc_with_related_concepts

from dfs.maprdb.struct_cond_related_concepts a

left join 
(select drugbank_id, flatten(cond_assoc_with) as cond_assoc_with_flat from dfs.maprdb.struct_cond_related_concepts) b
on a.drugbank_id = b.drugbank_id;


create or replace view dfs.maprdb.struct_ind_final as

select distinct a.drugbank_id, a.condition_meddra_id, a.cond_mod_of_meddra_id, a.rel_cond_meddra_id, 
a.cond_assoc_with_meddra_id, a.cond_assoc_with_mod_of_meddra_id, 
b.cond_assoc_with_rel_conc_flat.meddra_id as cond_assoc_with_rel_conc_meddra_id

from dfs.maprdb.struct_cond_assoc_with a

left join 
(select drugbank_id, flatten(cond_assoc_with_related_concepts) as cond_assoc_with_rel_conc_flat from dfs.maprdb.struct_cond_assoc_with) b
on a.drugbank_id = b.drugbank_id;


create table dfs.maprdb.drugbank_meddra_ids (drugbank_id, meddra_id) as
select distinct drugbank_id, meddra_id from
(select drugbank_id, condition_meddra_id as meddra_id
from dfs.maprdb.struct_ind_final
where condition_meddra_id is not null
union
select drugbank_id, cond_mod_of_meddra_id as meddra_id
from dfs.maprdb.struct_ind_final
where cond_mod_of_meddra_id is not null
union
select drugbank_id, rel_cond_meddra_id as meddra_id
from dfs.maprdb.struct_ind_final
where rel_cond_meddra_id is not null
union
select drugbank_id, cond_assoc_with_meddra_id as meddra_id
from dfs.maprdb.struct_ind_final
where cond_assoc_with_meddra_id is not null
union
select drugbank_id, cond_assoc_with_mod_of_meddra_id as meddra_id
from dfs.maprdb.struct_ind_final
where cond_assoc_with_mod_of_meddra_id is not null
union
select drugbank_id, cond_assoc_with_rel_conc_meddra_id as meddra_id
from dfs.maprdb.struct_ind_final
where cond_assoc_with_rel_conc_meddra_id is not null);
