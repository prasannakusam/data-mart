use dfs.maprdb;

create or replace view dfs.maprdb.v_pubmed_meshhead as
select a.pubmed_id,
a.article_title,
a.authors,
a.abstract_text,
a.journal_title,
a.article_date,
a.publication_type,
a.chemicals,
b.meshheadings_flat.descriptorname as descriptor_name,
b.meshheadings_flat.qualifiername as qualifier_name
from dfs.maprdb.pubmed as a
left join
(select pub.pubmed_id, flatten(pub.meshheadings) as meshheadings_flat
from dfs.maprdb.pubmed pub) b
on a.pubmed_id = b.pubmed_id;

create or replace view dfs.maprdb.v_pubmed_mesh_qual as
select a.*, b.mesh_qual_flat
from dfs.maprdb.v_pubmed_meshhead a
left join
(select pub.pubmed_id, flatten(pub.qualifier_name) as mesh_qual_flat
from dfs.maprdb.v_pubmed_meshhead pub) b
on a.pubmed_id = b.pubmed_id;

create or replace view dfs.maprdb.v_pubmed_mesh_desc as
select a.*, b.mesh_desc_flat
from dfs.maprdb.v_pubmed_mesh_qual a
left join
(select pub.pubmed_id, flatten(pub.descriptor_name) as mesh_desc_flat
from dfs.maprdb.v_pubmed_mesh_qual pub) b
on a.pubmed_id = b.pubmed_id;

create or replace view dfs.maprdb.v_pubmed_chemicals as
select a.*, b.mesh_chem_flat
from dfs.maprdb.v_pubmed_mesh_desc a
left join
(select pub.pubmed_id, flatten(pub.chemicals) as mesh_chem_flat
from dfs.maprdb.v_pubmed_mesh_desc pub) b
on a.pubmed_id = b.pubmed_id;

create or replace view dfs.maprdb.v_pubmed_pubtype as
select a.*, b.mesh_pubtype_flat
from dfs.maprdb.v_pubmed_chemicals a
left join
(select pub.pubmed_id, flatten(pub.publication_type) as mesh_pubtype_flat
from dfs.maprdb.v_pubmed_chemicals pub) b
on a.pubmed_id = b.pubmed_id;

create or replace view dfs.maprdb.v_pubmed_abstract as
select a.*, b.mesh_abstract_flat
from dfs.maprdb.v_pubmed_pubtype a
left join
(select pub.pubmed_id, flatten(pub.abstract_text) as mesh_abstract_flat
from dfs.maprdb.v_pubmed_pubtype pub) b
on a.pubmed_id = b.pubmed_id;

create or replace view dfs.maprdb.v_pubmed_authors as
select a.*, b.mesh_authors_flat
from dfs.maprdb.v_pubmed_abstract a
left join
(select pub.pubmed_id, flatten(pub.authors) as mesh_authors_flat
from dfs.maprdb.v_pubmed_abstract pub) b
on a.pubmed_id = b.pubmed_id;