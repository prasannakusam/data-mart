import argparse
import json

import psycopg2
from pydrill.client import PyDrill
from pydrill.exceptions import ImproperlyConfigured

import pandas as pd


def parse_arguments():
    parser = argparse.ArgumentParser(description='Standardize data types in pubmed json schema')
    parser.add_argument("datamart", help="Please specify the data mart", type=str)
    return parser.parse_args()


def getPostgresConnection():
    try:
        conn = psycopg2.connect(host='pg.vertiv.net', user='postgres', password='pos123', dbname='medmeme')
        return conn
    except Exception as e:
        return e


def getDrillConnection():
    drill = PyDrill(host='localhost', port=8047)
    if not drill.is_active():
        raise ImproperlyConfigured('Please run Drill first')
    return drill


if __name__ == '__main__':

    conn = getPostgresConnection()
    cur = conn.cursor()
    datamart = parse_arguments().datamart
    cur.execute("SELECT martid from datamartmaster where martname = '%s'" % (datamart))

    martids = cur.fetchall()

    for martid in martids:
        cur.execute("SELECT \"statement\", statementseq from datamartmeta where martid = '%s'" % (martid[0]))

        df = pd.DataFrame(cur.fetchall(), columns=['statement', 'statementseq'])
        df = df.sort_values(by=['statementseq'])

        sqlQueries = df['statement'].tolist()

        drill = getDrillConnection()

        for query in sqlQueries:
            drill.query(query)

    conn.close()
