import argparse
import errno
import json
import logging
import os
import shutil
import sys
from os import path

from boto3 import Session


def parse_arguments():
    parser = argparse.ArgumentParser(description='Standardize data types in pubmed json schema')
    parser.add_argument("source_path", help="Please specify path to pubmed input files to process", type=str)
    parser.add_argument("output_path", help="Please specify output path to write standardized pubmed files", type=str)
    return parser.parse_args()


def get_s3_client():
    session = Session(
        aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
        aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY')
    )

    return session.client('s3')


def get_logger(filename, log_file=None):
    logger = logging.getLogger(filename)
    logger.setLevel(logging.INFO)

    formatter = logging.Formatter('%(asctime)s - [%(name)s:%(lineno)d] - %(levelname)s - %(message)s')

    channel = logging.FileHandler(log_file) if log_file else \
        logging.StreamHandler(sys.stdout)
    channel.setLevel(logging.INFO)
    channel.setFormatter(formatter)

    logger.addHandler(channel)

    return logger


logger = get_logger(__file__)
args = parse_arguments()
source_path = path.join(args.source_path, '')
output_path = path.join(args.output_path, '')


def write_to_file(content, target_dir, file_name):
    try:
        os.makedirs(target_dir)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    target_file = path.join(target_dir, file_name)
    with open(target_file, 'w') as outfile:
        json.dump(content, outfile)


shutil.rmtree(output_path, ignore_errors=True)

def main():

    s3_client = get_s3_client()

    s3_client

    response = s3_client.list_object(Bucket='', Prefix='')

    for file in os.listdir(source_path):
        source_file = source_path + file
        if os.path.isfile(source_file) and not file.startswith('.'):
            data = ''
            try:
                with open(source_file, 'r', encoding='utf8') as input_file:
                    data = json.loads(input_file.read())
                    newData = {}

                    medlineCitation = data['PubmedArticle']['MedlineCitation']
                    article = medlineCitation['Article']

                    newData['pubmed_id'] = medlineCitation['PMID']['text']

                    if 'ArticleTitle' in article:
                        newData['article_title'] = article['ArticleTitle']

                    if 'Abstract' in article and 'AbstractText' in article['Abstract']:
                        abstractText = article['Abstract']['AbstractText']
                        if not isinstance(abstractText, list):
                            if isinstance(abstractText, str):
                                abstractText = [{"Label": "UNLABELLED", "text": abstractText}]
                            elif isinstance(abstractText, dict):
                                abstractText = [{"Label": "UNLABELLED", "text": abstractText['text']}]

                        newData['abstract_text'] = abstractText

                    if 'AuthorList' in article:
                        authors = article['AuthorList']['Author']
                        if not isinstance(authors, list):
                            if 'AffiliationInfo' in authors:
                                affiliationInfo = authors['AffiliationInfo']
                                if not isinstance(affiliationInfo, list):
                                    authors['AffiliationInfo'] = [affiliationInfo]
                            newData['authors'] = [authors]
                        else:
                            for i, f in enumerate(authors):
                                if 'AffiliationInfo' in f:
                                    affiliationInfo = f['AffiliationInfo']
                                    if not isinstance(affiliationInfo, list):
                                        authors[i]['AffiliationInfo'] = [affiliationInfo]
                            newData['authors'] = authors

                    if 'Journal' in article and 'Title' in article['Journal']:
                        newData['journal_title'] = article['Journal']['Title']

                    if 'ArticleDate' in article:
                        newData['article_date'] = article['ArticleDate']

                    if 'PublicationTypeList' in article and 'PublicationType' in article['PublicationTypeList']:
                        publication_type = article['PublicationTypeList']['PublicationType']
                        if not isinstance(publication_type, list):
                            publication_type = [publication_type]
                        newData['publication_type'] = publication_type

                    if 'MeshHeadingList' in medlineCitation:

                        meshHeading = medlineCitation['MeshHeadingList']['MeshHeading']

                        if not isinstance(meshHeading, list):
                            meshHeading = [meshHeading]

                        for i, f in enumerate(meshHeading):

                            if 'QualifierName' in f:
                                if not isinstance(f["QualifierName"], list):
                                    meshHeading[i]["QualifierName"] = [f['QualifierName']]

                            if 'DescriptorName' in f:
                                if not isinstance(f['DescriptorName'], list):
                                    meshHeading[i]["DescriptorName"] = [f['DescriptorName']]
                        newData['meshheadings'] = meshHeading

                    if 'ChemicalList' in medlineCitation and 'Chemical' in medlineCitation['ChemicalList']:
                        chemicals = medlineCitation['ChemicalList']['Chemical']
                        if not isinstance(chemicals, list):
                            chemicals = [chemicals]
                        newData['chemicals'] = chemicals

                    write_to_file(newData, output_path, file)

            except Exception as e:
                logger.info("Unable to process file: {} : {}".format(file, e, e.with_traceback()))


if __name__ == '__main__':
    main()