# -*- coding: utf-8 -*-
"""
Created on Wed May  2 14:27:20 2018

@author: Dastgeer Shaikh
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 16 15:28:04 2018

@author: Dr Dastgeer Shaikh
objective - The code reads multiple schema changes in the dailymed
files and creates a sub-data sctructure for the purpose of
querying with Apache drill on Mapr database
Reason - Apache drill does not support schema change queries due to
exception error.
This utility is a way around.
The final updated json output of this utility to be uploaded
on Mapr then query with apache drill

"""

import os
import json
import time
from pandas.io.json import json_normalize


def flatten_json(y):
    out = {}

    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '_')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + '_')
                i += 1
        else:
            out[name[:-1]] = x

    flatten(y)
    return out


def traverse(obj):
    if isinstance(obj, dict):
        return {k: traverse(v) for k, v in obj.items()}
    elif isinstance(obj, list):
        return [traverse(elem) for elem in obj]
    else:
        return obj  # no container, just values (str, int, float)
    

def output_json(r, filename):
    r = json.dumps(r)
    
    with open(filename, 'w') as f:
        f.write(r)
    
    return r

def read_json(fname):
    
    jsonFile = open(fname, 'r')
    values = json.load(jsonFile)
    jsonFile.close()  
    
    return values


def unflatten(dictionary):
    resultDict = dict()
    #for key, value in dictionary.iteritems():
    for key, value in dictionary.items():
        parts = key.split("_")
        d = resultDict
        for part in parts[:-1]:
            if part not in d:
                d[part] = dict()
            d = d[part]
        d[parts[-1]] = value
    return resultDict



# ===================================================   
   
if __name__ == '__main__':
    startTime = time.time()

    dirPath2 = '/Users/prasannakusam/MedMeme/Data/pubmed/source/'
    outDir = '/Users/prasannakusam/MedMeme/Data/pubmed/processed/'
    
    # use input for query/tag extraction
    mycode = ['34072-9', '43685-7', '34084-4', '42232-9', '34071-1'] 
    
        
  
    # use input for query/tag extraction
    
    for file in os.listdir(dirPath2):   
        print(file)
        
        FileName = dirPath2+file
 
        # read json and save into values
        values = read_json(FileName)
     

        kv = values.items()

        for key, value in kv:
            print ( key, 'is:', value)
       
    
        #kv = traverse(value.items())

        flat_dict = flatten_json(value)
        json_normalize(flat_dict)

        useme_dict = dict()
        for key, item in values.items():
        
            if isinstance (key, str):
                #print(key)
                useme_dict[key] = item
                        
                # export as "REG/NESTED" json
                reg_dict = unflatten(useme_dict)
                mydict=traverse(reg_dict)
                flat_json = flatten_json(mydict)
    

        # creat sub
        dict_apache_drill_kv = dict()
        for key, value in flat_json.items():
            if value in mycode:
                
                try:
                    dict_apache_drill_kv[key]   = value
                    dict_apache_drill_kv[mykey] = flat_json[mykey]
                except (RuntimeError, TypeError, NameError, KeyError):
                    pass
           
               
        # export as "sub structured" json
        filename = 'dm_'+flat_json['document_id_root']  + '_SUB'+ '.json'
        Flat_json = output_json(flat_json, outDir+filename)    

