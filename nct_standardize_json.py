import errno
import json
import logging
import os
import sys
from os import path

def parse_arguments():
   parser = argparse.ArgumentParser(description='Standardize data types in NCT json schema')
   parser.add_argument("source_path", help="Please specify path to NCT input files to process", type=str)
   parser.add_argument("output_path", help="Please specify output path to write standardized NCT files", type=str)
   return parser.parse_args()


def get_logger(filename, log_file=None):
    logger = logging.getLogger(filename)
    logger.setLevel(logging.INFO)

    formatter = logging.Formatter('%(asctime)s - [%(name)s:%(lineno)d] - %(levelname)s - %(message)s')

    channel = logging.FileHandler(log_file) if log_file else \
        logging.StreamHandler(sys.stdout)
    channel.setLevel(logging.INFO)
    channel.setFormatter(formatter)

    logger.addHandler(channel)

    return logger


logger = get_logger(__file__)
#args = parse_arguments()
#source_path = path.join(args.source_path, '')
#output_path = path.join(args.output_path, '')

source_path = "/Users/prasannakusam/MedMeme/Data/NCT/test/"
output_path = "/Users/prasannakusam/MedMeme/Data/NCT/processed-test/"

def write_to_file(content, target_dir, file_name):
    try:
        os.makedirs(target_dir)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    target_file = path.join(target_dir, file_name)
    with open(target_file, 'w') as outfile:
        json.dump(content, outfile)

for file in os.listdir(source_path):
    source_file = source_path + file
    print(source_file)
    if os.path.isfile(source_file) and not file.startswith('.'):
        data = ''
        try:
            with open(source_file, 'r', encoding='utf8') as input_file:
                
                data = json.loads(input_file.read())

                newData = {}

                clinical_study = data['clinical_study']                

                newData['nct_id'] = clinical_study['id_info']['nct_id']
                
                if 'phase' in clinical_study:
                    newData['phase'] = clinical_study['phase']

                if 'sponsors' in clinical_study:

                    sponsors = clinical_study['sponsors']

                    if not isinstance(sponsors, list):
                        sponsors = [sponsors]

                    for i, f in enumerate(sponsors):

                        if 'lead_sponsor' in f:
                            if not isinstance(f['lead_sponsor'], list):
                                sponsors[i]["lead_sponsor"] = [f['lead_sponsor']]

                        if 'collaborator' in f:
                            if not isinstance(f['collaborator'], list):
                                sponsors[i]["collaborator"] = [f['collaborator']]
                    newData['sponsors'] = sponsors


                if 'enrollment' in clinical_study:
                    enrollment = clinical_study['enrollment']
                    
                    mydict = dict()
                    
                    if isinstance (enrollment, str):
                        mydict['text'] = enrollment
                        enrollment = [mydict]
                    if isinstance(enrollment, dict):
                        enrollment = [enrollment]  
                    
                    newData['enrollment'] = enrollment

                # if 'intervention' in clinical_study:
                #     intervention = clinical_study['intervention']
                #
                #     for i, d in enumerate(intervention):
                #         newDict = {'intervention_type': d['intervention_type'],
                #                    'intervention_name': d['intervention_name']}
                #         intervention[i] = newDict
                #
                #     newData['intervention'] = intervention
                #     print(newData['intervention'])

                if 'intervention' in clinical_study:
                    intervention = clinical_study['intervention']

                    for d in clinical_study['intervention']:
                        mydict = dict()
                        filter_keys = ('intervention_type', 'intervention_name')
                        key_value = dict(filter(lambda i: i[0] in filter_keys, d.items()))

                        clinical_study['intervention'] = key_value

                        intervention = clinical_study['intervention']

                        newData['intervention'] = [intervention]
                        print(newData['intervention'])
                    
                if 'study_design_info' in clinical_study:
                    study_design_info = clinical_study['study_design_info']
                  
                    if not isinstance(study_design_info, list):
                        study_design_info = [study_design_info]
                    
                    newData['study_design_info'] = study_design_info
                    
                if 'official_title' in clinical_study:
                    official_title = clinical_study['official_title']
                    
                    newData['official_title'] = official_title

                if 'clinical_results' in clinical_study:
                    clinical_results = clinical_study['clinical_results']
                    
                    if 'reported_events' in clinical_results:

                        reported_events = clinical_results['reported_events']
                        
                        if not isinstance(reported_events, list):
                            reported_events = [reported_events]

                        for i, f in enumerate(reported_events):
    
                            if 'group_list' in f:
                                        
                                if not isinstance(f['group_list'], list):
                                    reported_events[i]['group_list'] = [f['group_list']]
                                    group=f['group_list'][i]['group']                  
                                    
                                    if 'group' in f['group_list'][i]:
                                        if not isinstance(group, list):
                                            group = [group]
                                
                            if 'serious_events' in f:

                                if not isinstance(f['serious_events'], list):
                                    reported_events[i]['serious_events'] = [f['serious_events']]

                              
                                if 'category' in f['serious_events'][i]['category_list']:

                                    category = f['serious_events'][i]['category_list']['category']
                                    
                                    if not isinstance(category, list):
                                        f['serious_events'][i]['category_list']['category'] = [category]

                                    if 'event' in f['serious_events'][i]['category_list']['category'][i]['event_list']:
                                        
                                        event=f['serious_events'][i]['category_list'][
                                                    'category'][i]['event_list']['event']

                                        if not isinstance(event, list):
                                            f['serious_events'][i]['category_list']['category'][i][
                                                    'event_list']['event']=[event]

                                            counts=f['serious_events'][i]['category_list'][
                                                     'category'][i]['event_list'][
                                                             'event'][i]['counts']
                                            
                                            if not isinstance(counts,list):
                                                f['serious_events'][i]['category_list'][
                                                                'category'][i]['event_list'][
                                                                    'event'][i]['counts']= [counts]                                                
                                        
                                if 'other_events' in f:

                                    if not isinstance(f['other_events'], list):
                                        reported_events[i]['other_events'] = [f['other_events']]
                                        
                                        if 'category' in f['other_events'][i]['category_list']:
                                            category = f['other_events'][i]['category_list']['category']

                                            if not isinstance(category, list):
                                                f['other_events'][i]['category_list']['category'] = [category]
                                                                                    
                                            if 'event' in f['other_events'][i]['category_list']['category'][i]['event_list']:
                                                
                                            
                                                    event=f['other_events'][i]['category_list'][
                                                                'category'][i]['event_list']['event']
                                                   
                                                    if not isinstance(event, list):
                                                            f['other_events'][i]['category_list'][
                                                                'category'][i]['event_list']['event']=[event]
                                                
                                               
                                                    counts=f['other_events'][i]['category_list'][
                                                                'category'][i]['event_list'][
                                                                    'event'][i]['counts']
                                                    if isinstance(counts,dict):
                                                        f['other_events'][i]['category_list'][
                                                                'category'][i]['event_list'][
                                                                    'event'][i]['counts']= [counts]      
                                        
                    newData['reported_events'] = reported_events
                    
                write_to_file(newData, output_path, file)

        except Exception:
            pass  